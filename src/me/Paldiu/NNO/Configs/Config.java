package me.Paldiu.NNO.Configs;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import me.Paldiu.NNO.JFLog;
import me.Paldiu.NNO.Main;
import me.Paldiu.NNO.Util;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.util.FileUtil;

public class Config
{
    private static Map<String, PlayerConfig> playerList = new HashMap<String, PlayerConfig>();
    private static List<String> playerNames = new ArrayList<String>();

    private Config()
    {
        throw new AssertionError();
    }

    public static List<String> getPlayerNames()
    {
        return playerNames;
    }

    public static void loadPlayerList()
    {
        try
        {
            playerList.clear();

            Util.createDefaultConfiguration(Main.PLAYERDATA_FILE, Main.plugin_file);
            FileConfiguration config = YamlConfiguration.loadConfiguration(new File(Main.plugin.getDataFolder(), Main.PLAYERDATA_FILE));

            if (config.isConfigurationSection("players"))
            {
                ConfigurationSection section = config.getConfigurationSection("players");

                for (String player_name : section.getKeys(false))
                {
                    PlayerConfig player = new PlayerConfig(player_name, section.getConfigurationSection(player_name));
                    playerList.put(player_name.toLowerCase(), player);
                }
            }
            else
            {
                JFLog.warning("Missing players section in player.yml.");
            }

            updateIndexLists();
        }
        catch (Exception ex)
        {
            JFLog.severe(ex);
        }
    }

    public static void backupSavedList()
    {
        File a = new File(Main.plugin.getDataFolder(), Main.PLAYERDATA_FILE);
        File b = new File(Main.plugin.getDataFolder(), Main.PLAYERDATA_FILE + ".bak");
        FileUtil.copy(a, b);
    }

    public static void updateIndexLists()
    {
        playerNames.clear();

        Iterator<Entry<String, PlayerConfig>> it = playerList.entrySet().iterator();
        while (it.hasNext())
        {
            Entry<String, PlayerConfig> pair = it.next();

            String player_name = pair.getKey().toLowerCase();
            PlayerConfig player = pair.getValue();
        }

        playerNames = Util.removeDuplicates(playerNames);
    }

    public static void savePlayerList()
    {
        try
        {
            updateIndexLists();

            YamlConfiguration config = new YamlConfiguration();

            Iterator<Entry<String, PlayerConfig>> it = playerList.entrySet().iterator();
            while (it.hasNext())
            {
                Entry<String, PlayerConfig> pair = it.next();

                String player_name = pair.getKey().toLowerCase();
                PlayerConfig player = pair.getValue();
                config.set("players." + player_name + ".custom_join_message", player.getCustomJoinMessage());
                config.set("players." + player_name + ".custom_login_message", player.getCustomLoginMessage());
            }

            config.save(new File(Main.plugin.getDataFolder(), Main.PLAYERDATA_FILE));
        }
        catch (Exception ex)
        {
            JFLog.severe(ex);
        }
    }

    public static PlayerConfig getPlayerEntry(String player_name)
    {
        player_name = player_name.toLowerCase();

        if (playerList.containsKey(player_name))
        {
            return playerList.get(player_name);
        }
        else
        {
            return null;
        }
    }

    public static PlayerConfig getPlayerEntry(Player p)
    {
        return getPlayerEntry(p.getName().toLowerCase());
    }

    public static void addPlayer(String player_name)
    {
        try
        {
            player_name = player_name.toLowerCase();

            if (playerList.containsKey(player_name))
            {
                PlayerConfig player = playerList.get(player_name);
            }
            else
            {
                String custom_login_message = "";
                String custom_join_message = "";
                PlayerConfig player = new PlayerConfig(player_name, custom_login_message, custom_join_message);
                playerList.put(player_name.toLowerCase(), player);
            }

            savePlayerList();
        }
        catch (Exception ex)
        {
           JFLog.severe(ex);
        }
    }

    public static void addPlayer(Player p)
    {
        String player_name = p.getName().toLowerCase();
        addPlayer(player_name);
    }

    public static void removePlayer(String player_name)
    {
        try
        {
            player_name = player_name.toLowerCase();

            if (playerList.containsKey(player_name))
            {
                PlayerConfig player = playerList.get(player_name);
                savePlayerList();
            }
        }
        catch (Exception ex)
        {
            JFLog.severe(ex);
        }
    }

    public static void removePlayer(Player p)
    {
        removePlayer(p.getName());
    }

    public static void cleanPlayerList(boolean verbose)
    {
        try
        {
            Iterator<Entry<String, PlayerConfig>> it = playerList.entrySet().iterator();
            while (it.hasNext())
            {
                Entry<String, PlayerConfig> pair = it.next();
                PlayerConfig player = pair.getValue();
            }
            savePlayerList();
        }
        catch (Exception ex)
        {
            JFLog.severe(ex);
        }
    }
}
